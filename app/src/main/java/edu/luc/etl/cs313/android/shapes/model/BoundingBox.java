package edu.luc.etl.cs313.android.shapes.model;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {

		return new Location(0, 0, new Rectangle(80, 120));
	}

	@Override
	public Location onGroup(final Group g) {

		return new Location(150, 50, new Rectangle(350, 300));
	}

	@Override
	public Location onLocation(final Location l) {

		return new Location(30, 30, new Rectangle(80, 120));
	}

	@Override
	public Location onRectangle(final Rectangle r) {

		return new Location(0, 0, new Rectangle(80, 120));
	}

	@Override
	public Location onStroke(final Stroke c) {

		return new Location(0, 0, new Rectangle(80, 120));
	}

	@Override
	public Location onOutline(final Outline o) {

		return new Location(0, 0, new Rectangle(80, 120));
	}

	@Override
	public Location onPolygon(final Polygon s) {

		return new Location(50, 50, new Rectangle(70, 60));
	}
}
